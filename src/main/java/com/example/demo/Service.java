package com.example.demo;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
	
	private List<Entity> list= new ArrayList<Entity>();
	
	public List getAll() {
		return list;
	}
	
	public int getOTP(int number) {
		
		for(Entity e:list) {
			if((e.getPhoneNum()==number))
				return -1;
		}
		
		Entity e = new Entity(number);
		list.add(e);
		return e.getOTP();
	}
	
	public String checkOTP(int number, int otp) {
		for(Entity e:list) {
			if((e.getPhoneNum()==number) && (e.getOTP()==otp))
				return "valid";
			else if(((e.getPhoneNum()==number) && (e.getOTP()!=otp)))
				return "Invalid";
				
		}
		return "Not Found";
	}
}
